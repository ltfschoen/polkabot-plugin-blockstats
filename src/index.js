#!/usr/bin/env node

import BN from 'bn.js'
import pluginConfig from './config'

module.exports = class BlocsStats {
  constructor (pbot) {
    this.pbot = pbot
    this.config = pluginConfig
    this.data = []
    this.previousData = null
  }

  start () {
    console.log('BlocksStats - Starting with config:', this.config);
    this.watchChain().catch((error) => {
      console.error('BlocksStats - Error subscribing to chain head: ', error);
    });
  }

  async watchChain () {
    // Reference: https://polkadot.js.org/api/examples/promise/02_listen_to_blocks/
    await this.pbot.polkadot.rpc.chain
      .subscribeNewHead((header) => {
        // FIXME - is this issue still an issue. see https://github.com/polkadot-js/api/issues/142
        if (header) {
          console.log(`BlocksStats - Chain is at block: #${header.blockNumber}`);
          const bnBlockNumber = new BN(header.blockNumber, 16)
          // console.log('#' + bnBlockNumber.toString(10))

          this.addBlock(header)

          if (bnBlockNumber.mod(new BN(this.config.NB_BLOCKS)).toString(10) === '0') {
            this.computeStats()
            this.alert(bnBlockNumber)
          }
        }
      })
  }

  addBlock (header) {
    const data = {
      tmsp: new Date(),
      blockTime: this.previousData ? new Date() - this.previousData.tmsp : null,
      header
    }
    this.data.push(data)

    while (this.data.length > this.config.NB_BLOCKS) { this.data.shift() }
    this.previousData = data
  }

  averageBlockTime () {
    const sum = (accumulator, currentValue) => accumulator + currentValue
    return this.data
      .map(el => el.blockTime || 0)
      .reduce(sum) / this.data.filter(item => item.blockTime > 0).length / 1000
  }

  computeStats () {
    this.stats = {
      nbBlock: this.data.length,
      averageBlockTime: this.averageBlockTime()
    }
  }

  alert (bnBlockNumber) {
    if (this.stats.averageBlockTime >= pluginConfig.threshold) {
      this.pbot.matrix.sendTextMessage(
        this.pbot.config.matrix.roomId,
        `WARNING: Average block time exceeded ${pluginConfig.threshold.toFixed(3)}s
Stats for the last ${pluginConfig.NB_BLOCKS} at #${bnBlockNumber.toString(10)}:
    - Nb Blocks: ${this.stats.nbBlock}
    - Average Block time: ${this.stats.averageBlockTime.toFixed(3)}s`)
        .finally(function () {})
    }
  }
}
