const config = {
  NB_BLOCKS: 300, // Every how many blocks do we output stats
  threshold: 6.0 // We remain silent unless the average goes above this value
}

export default config
